// OPERATORS

// Assignment Operators
// Basic Assignment Operator(=)

let assignmentNumber = 8;
// Arithmetic Assignment Operators
// Addition 

assignmentNumber = assignmentNumber + 2;
console.log("Result:" + assignmentNumber);
assignmentNumber += 2;
console.log("Result 2:" + assignmentNumber);

// Subtraction

assignmentNumber -= 2;
console.log("Result (-= 2):" + assignmentNumber);

assignmentNumber *= 2;
console.log("Result (*= 2):" + assignmentNumber);

assignmentNumber /= 2;
console.log("Result ( /= 2):" + assignmentNumber);

// Arithmetic Operators
let x = 1397;
let y = 7831;

let sum = x + y;
console.log("Result (add = x + y):" + sum);

let sub = x - y;
console.log("Result (sub = x - y):" + sub);

let mul = x * y;
console.log("Result (mul = x * y):" + mul);

let div = x / y;
console.log("Result (div = x / y):" + div);

let mod = x % y;
console.log("Result (mod = x % y):" + mod);

// Multiple opearators and Parentheses

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result (mdas = 1 + 2 - 3 * 4 / 5;):" + mdas);

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result (pemdas = 1 + (2 - 3) * (4 / 5)):" + pemdas);

// Increment and Decrement : (PRE: ++var = var + 1)
let z = 3;
let w = 3;
let i = 3;
let h = 3;

console.log("Result (z = 3):" + z);
let incrementPre = ++z;
 console.log("Result (increment = ++z):" + incrementPre);
let incrementPost = w++;
 console.log("Result (incrementPost = w++;):" + incrementPost);

let deccrementPre = --i;
 console.log("Result (deccrementPre = --i):" + deccrementPre);
 let deccrementPost = h--;
 console.log("Result (deccrementPost = h--):" + deccrementPost);



 // Type Coercion
 let numA = 10;
 let numB = "12";

 let coercion = numA + numB;
 console.log(coercion);
 console.log(typeof coercion);

let numC = false + 1;
console.log(numC);


// Relational Operators>Equality Operators (==)
let juan = 'juan';
console.log(1 == 1);
console.log(1==2);
console.log(1=='1');
console.log('juan' == juan);

// Inequality Operator (!=)
console.log(1 != 1);
console.log(1!=2);
console.log(1!='1');
console.log('juan' != juan);

// <,>,<=,>=
console.log(4<6);
console.log(1>8);
console.log(5>=5);
console.log(10<=15);

// strict in/equality operator (===)

console.log(1==='1');
console.log(1!=='1');

// logical operators
let isLegalAge = true;
let isRegsitered = false;
// return true if all operands are true
let allrequirementsMet  = isLegalAge && isRegsitered;
console.log("Result (allrequirementsMet  = T && F)"+allrequirementsMet);
// OR(||)
let requirementsMet  = isLegalAge || isRegsitered;
console.log("Result (allrequirementsMet  = T || F)"+requirementsMet);


//Selection control structures
// if, else if and else statement

let numE = -1;

if(numE < 0){
    console.log("Hello");
}


let nickName = 'Mattheo';

if(nickName == 'Matt'){
    console.log("Hello" + nickName);
}

let numF = 1;

if(numF < 0){
    console.log("Hello");
}else if(numF == 0){
    console.log("World");
}else{
    console.log("Again");
}

let message = "No Message";
console.log(message);

function determineTyphoonIntensity(windSpeed){
    if (windSpeed < 30){
        return 'Not a typhoon yet.';
    } else if (windSpeed>=31 && windSpeed <= 60){
        return 'Tropical depression detected';
    }else if(windSpeed >= 62 && windSpeed <= 88){
        return 'Tropical storm detected';
    }else if(windSpeed >= 89 && windSpeed <= 117){
        return 'Severe tropical storm detected';
    }else{
        return 'Typhoon detected';
    }
}

message = determineTyphoonIntensity(29);
console.log(message);

// Ternary Operator: oneliner
let ternaryResult = ( 1 < 18 ) ? "wow" : "yes";
console.log("ternaryResult = ( 1 < 18 ):" + ternaryResult);

let name;
function isOfLegalAge(){
    name = 'John';
    return 'You are of the legal age limit';
}

function isUnderAge(){
    name = 'Jane'
    return "You are under the age limit";
}

let age = parseInt(prompt("WHat is your age?"));
console.log(age);

let name1 = prompt("WHat is your name?")
console.log(name1);

let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge(); //oneliner if-else
console.log('Result of Ternary Op function:  '+ legalAge+','+name1);

let department = '';
if(department === 'a' || department === 'A'){
    console.log("You are in department A");
}else if(department === 'b' || department === 'B'){
    console.log("You are in department A");
}

// Switch statement
let day = prompt("What day of the week is it today?").toLowerCase();

switch(day){
    case 'sunday':
        console.log("The day is red");
        break;
    case 'monday':
        console.log("The day is orange");
        break;
    case 'tuesday':
        console.log("The day is yellow");
        break;
    case 'wednesday':
        console.log("The day is green");
        break;
    case 'thursday':
        console.log("The day is blue");
        break;
    case 'friday':
        console.log("The day is indigo");
        break;
    case 'saturday':
        console.log("The day is violet");
        break;
    default:
        console.log("Please input a valid day");
}

// Try-Catch-Finally Statement

function showIntensityALert(windSpeed){
    try{
        // Attempt to execute a code
        alert(determineTyphoonIntensity(windSpeed));
    }
    // error/err are commonly used variable by developers for storing errors
    catch (error){
        console.log(typeof error);

        console.warn(error.message);
        // error.message use to access information relating to an error object.
    }
    finally{
        //continue on excuting the code regardless of success or failure of code execution in the 'try' block.
        alert('Intensity updates will show new alert.')
    }
}

showIntensityALert(56);





